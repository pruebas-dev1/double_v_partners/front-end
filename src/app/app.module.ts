import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { NgChartsModule } from 'ng2-charts';
// services
import { SwalAlertService } from './services/swal-alert.service';
// Guard
import { ScoreGuard } from './score.guard';
// Component
import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import { UserDetailComponent } from './components/users/detail/user-detail.component';
import { InputGroupSearchComponent } from './components/shared/bootstrap/input-group-search.component';
import { BarChartComponent } from './components/shared/chart/bar-chart/bar-chart.component';

const appRoutes: Routes = [
  { path: 'user/:login/:id/:score', component:UserDetailComponent, canActivate: [ScoreGuard] },
  { path: '', component:UsersComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    // Shared
    InputGroupSearchComponent,
    BarChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    NgChartsModule
  ],
  providers: [
    SwalAlertService,
    ScoreGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
