import { Component, EventEmitter, Input, Output } from "@angular/core";
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { SwalAlertService } from './../../../services/swal-alert.service';


@Component({
  selector: 'app-input-group-search',
  // <input type="text" class="form-control" placeholder="{{placeholder}}" [(ngModel)]="inputSearch">
  template: `<form [formGroup]="formBasicSearch">
    <div class="input-group">
      <input type="text" class="form-control" placeholder="{{placeholder}}" formControlName="inputSearch" required>
      <span class="input-group-text p-0 bg-transparent">
        <button type="button" class="btn {{btnClass}} rounded-0" (click)="searchUsers()">
          <span [innerText]="btnText"></span>
          <i class="{{btnIcon}} ms-2"></i>
        </button>
      </span>
    </div>
  </form>`,
})

export class InputGroupSearchComponent{

  formBasicSearch: FormGroup;
  constructor(private swalAlertService: SwalAlertService) {
    this.formBasicSearch = new FormGroup({
      inputSearch: new FormControl('', [Validators.required, Validators.minLength(4), this.textDifferent])
    });
  }

  inputSearch: string = '';
  @Output() clickSearch = new EventEmitter<string>();

  searchUsers(){
    if(this.formBasicSearch.status != "INVALID"){
      this.clickSearch.emit(this.formBasicSearch.value.inputSearch);
    }else{
      console.log('1212', this.formBasicSearch);
      this.swalAlertService.showAlertToast(
        'El usuario mínimo debe contener 4 caracteres y debe ser diferente a (doublevpartners)', 'warning'
      );
    }
  }

  @Input() placeholder:string = 'Usuario';
  @Input() btnClass:string = 'btn-outline-primary';
  @Input() btnText:string = 'Buscar';
  @Input() btnIcon:string = 'fa-solid fa-magnifying-glass';

  textDifferent(control: FormControl): { [s: string]: boolean } | null {
    const inputValue = control.value;
    if (inputValue === 'doublevpartners') {
      return { 'textDifferent': true };
    }    
    return null;
  }
}