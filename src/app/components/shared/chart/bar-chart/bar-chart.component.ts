import { Component, Input, ViewChild, OnInit, OnChanges, SimpleChanges } from '@angular/core';
// import { Component, Input, ViewChild, OnInit, OnChanges, SimpleChanges } from '@angular/core';
// import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { ChartConfiguration, ChartData, ChartEvent, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

import DataLabelsPlugin from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css'],
})
export class BarChartComponent implements OnInit {

  @Input() title: string = '...';
  @Input() datasetsBarChart: any[] = [];

  sTitleChart = this.title;
  aDatasetsBarChart = this.datasetsBarChart;

  // @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;
  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective | undefined;


  ngOnChanges(changes: SimpleChanges): void {
    this.title = changes['title'].currentValue;
    this.datasetsBarChart = changes['datasetsBarChart'].currentValue;
    console.log(this.title, this.datasetsBarChart);
    if( this.chart ){
      this.chart.update();
    }
  }

  public barChartData: ChartData<'bar'> = {
    labels: [ this.sTitleChart ],
    datasets: this.aDatasetsBarChart
  };

  ngOnInit(): void {
    console.log(1212, '1212');
    if( this.chart ){
      this.chart.update();
    }
  }

  public barChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {},
      y: {
        min: 10
      }
    },
    plugins: {
      legend: {
        display: true,
      },
      datalabels: {
        anchor: 'end',
        align: 'end'
      }
    }
  };

  public barChartType: ChartType = 'bar';
  public barChartPlugins = [
    DataLabelsPlugin
  ];

  // events
  public chartClicked({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    // console.log(event, active);
  }
  
  public randomize(): void {
    // Only Change 3 values
    this.barChartData.datasets[0].data = [
      Math.round(Math.random() * 100),
      59,
      80,
      Math.round(Math.random() * 100),
      56,
      Math.round(Math.random() * 100),
      40 ];

    this.chart?.update();
  }
}