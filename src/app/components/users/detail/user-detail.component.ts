import { Component, OnInit } from "@angular/core";
import { map } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
// services
import { UsersService } from './../../../services/users.service';
import { SwalAlertService } from './../../../services/swal-alert.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-users-detail',
  templateUrl: './user-detail.component.html',
})

export class UserDetailComponent{
  user = {
    login: '',
    avatar: '',
    name: '',
    company: '',
    email: ''
  };
  // user = {login:''};
  constructor(
    private http: HttpClient,
    private usersService: UsersService,
    private swalAlertService: SwalAlertService,
    private route: ActivatedRoute
  ){
    console.log('xxx', UsersService);
  }

  ngOnInit(){
    this.user.login = this.route.snapshot.params['login'];
    this.usersService.getUserDetail(this.user.login).subscribe((response) => {
      console.log(response);
        this.user = {
          login: (response.login) ? response.login: 'No registra',
          avatar: (response.avatar_url) ? response.avatar_url: 'No registra',
          name: (response.name) ? response.name: 'No registra',
          company: (response.company) ? response.company: 'No registra',
          email: (response.emai) ? response.emai: 'No registra'
        };
        console.log(this.user);
      },(error) => {
        console.error('Error al obtener los datos:', error);
      }
    );
  }

  moduleTitle = 'Detalle de usuario';
}