import { Component } from "@angular/core";
import { map } from 'rxjs';
// services
import { UsersService } from './../../services/users.service';
import { SwalAlertService } from './../../services/swal-alert.service';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
})

export class UsersComponent{

  users: any[] = [];
  datasetsBarChart: any[] = [];

  constructor(
    private http: HttpClient,
    private usersService: UsersService,
    private swalAlertService: SwalAlertService
  ){
    console.log('xxx', UsersService);
  }

  searchUser(value: string): void {
    this.usersService.getUsers(value).subscribe((responseUsers) => {
      console.log(responseUsers);
      if( responseUsers.items.length > 0 ){
        this.datasetsBarChart = [];
        // Recorrer usuarios
        responseUsers.items.forEach((element: any) => {
          // Recuperar seguidores
          this.usersService.getUserDetail(element.login).subscribe((oUser) => {
            this.datasetsBarChart.push({
              data: [oUser.followers], label: oUser.login
            });
          },(error) => {
            console.error('Error al obtener los seguidores:', error);
          });
        });
      }
      this.users = responseUsers.items;
    }, (error) => {
      console.error('Error al obtener los usuarios:', error);
    });
  }
  moduleTitle = 'Listado de usuarios';
}