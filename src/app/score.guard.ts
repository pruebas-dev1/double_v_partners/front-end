import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';


// services
import { HttpClient } from '@angular/common/http';
import { UsersService } from './services/users.service';
import { SwalAlertService } from './services/swal-alert.service';


@Injectable({
  providedIn: 'root'
})
export class ScoreGuard {

  userScore = 0;

  constructor(
    private route: ActivatedRoute,
    private swalAlertService: SwalAlertService,
  ){
    console.log('xxx', UsersService);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let bReturn = true;
    if( parseInt(route.params['score']) < 30 ){
      this.swalAlertService.showAlertToast('Lo sentimos su score es menor a 30.', 'warning');
      bReturn = false;
    }
    console.log(parseInt(route.params['score']));
    return bReturn;
  }
};

