import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class UsersService{

  private apiUrl = 'https://api.github.com/';

  constructor(private http: HttpClient) {}

  getUsers(user:string){
    const url = `${this.apiUrl}search/users?q=${user}&page=1&per_page=10`;
    console.log(url);
    return this.http.get<any>(url);
  }

  getUserDetail(user:string){
    const url = `${this.apiUrl}users/${user}`;
    console.log(url);
    return this.http.get<any>(url);
  }

}
